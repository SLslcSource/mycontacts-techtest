package com.runtime.langs.app.mycontacts;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.runtime.langs.app.mycontacts.service.ContactModel;

public class DetailsActivity extends AppCompatActivity {

    public final String TAG = DetailsActivity.class.getSimpleName();
    public static final String EXTRA_LANGVO = "lanVO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        ContactModel extra = getIntent().getExtras().getParcelable(EXTRA_LANGVO);
        String formattedName = String.format(this.getString(R.string.name_surname), extra.first_name, extra.last_name);
        getSupportActionBar().setTitle(formattedName);
        ((TextView) findViewById(R.id.textViewName)).setText(formattedName);
        ((TextView) findViewById(R.id.textView)).setText(extra.description);
        ((TextView) findViewById(R.id.textViewSubtitle)).setText(extra.email);

        Glide
            .with(this)
            .load(extra.photo)
            .into(((ImageView) findViewById(R.id.profile_image)));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, extra.email);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));

                startActivity(Intent.createChooser(intent, getString(R.string.send_it_title)));
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
