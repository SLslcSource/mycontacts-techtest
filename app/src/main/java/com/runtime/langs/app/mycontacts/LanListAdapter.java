package com.runtime.langs.app.mycontacts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.runtime.langs.app.mycontacts.service.ContactsPayload;
import com.runtime.langs.app.mycontacts.service.ContactModel;

import java.util.ArrayList;

public class LanListAdapter extends RecyclerView.Adapter<LanListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ContactModel> langList;

    public LanListAdapter(Context mContext, ContactsPayload langList) {
        this.mContext = mContext;
        this.langList = langList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new LanListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactModel address = langList.get(position);
        holder.bindView(address);
    }

    @Override
    public int getItemCount() {
        return langList == null ? 0 : langList.size();
    }


    public void onItemSelect(ContactModel contactModel) {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        private TextView textViewSubTitle;
        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            textViewSubTitle = (TextView) itemView.findViewById(R.id.textViewSubtitle);
            imageView = (ImageView) itemView.findViewById(R.id.profile_image);

        }

        public void bindView(ContactModel contactModel) {

            textView.setText(String.format(mContext.getString(R.string.name_surname), contactModel.first_name, contactModel.last_name));
            textViewSubTitle.setText(contactModel.email);

            Glide
                .with(mContext)
                .load(contactModel.photo)
                .into(imageView);

            itemView.setOnClickListener(v -> {
                onItemSelect(contactModel);
            });
        }
    }
}
