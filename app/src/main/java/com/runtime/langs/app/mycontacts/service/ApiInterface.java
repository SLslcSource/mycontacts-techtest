package com.runtime.langs.app.mycontacts.service;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiInterface {

    // These Headers can also be dynamically set Intro the Api service via using retrofits Http Interceptor
    @Headers("secret-key: $2a$10$BiBCEhkICpfX8tEoVx1COe4raB/1XsuLdrtBLFVfKBvlaU4uq2PVu")
    @GET("5cbed5228967d56779977be6")
    Single<ContactsPayload> listContacts();

}
