package com.runtime.langs.app.mycontacts.service;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactModel implements Parcelable {

    public String id;
    public String description;
    public String photo;
    public String email;
    public String last_name;
    public String first_name;


    protected ContactModel(Parcel in) {
        id = in.readString();
        description = in.readString();
        photo = in.readString();
        email = in.readString();
        last_name = in.readString();
        first_name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(description);
        dest.writeString(photo);
        dest.writeString(email);
        dest.writeString(last_name);
        dest.writeString(first_name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };
}
